#!/usr/bin/env python3
from canvashelper import CanvasHelper
from grader import PythonGrader
from grader import TestSuite
import os
import util

DEFAULT_TEST_FILE = "testsuite.json"

# util.colored constants
TITLE = util.colored("Python Autograder for Canvas", "pink")
GOODBYE = util.colored("Thanks for using PAC! Goodbye :)", "pink")
SUCCESS = util.colored("SUCCESS", "green")
FAIL = util.colored("FAIL", "red")
MISSING = util.colored("MISSING", "red")
GRADE_UPLOAD_REPORT = util.colored("Grade Upload Report", "purple")
REPORT_MESSAGE = util.colored(
    "press [ENTER] for next grade result or [q] to quit ", "lightgreen")


def main():
    # Initial information collection
    print(TITLE)

    ch = CanvasHelper()
    course_selection = util.get_selection(ch.getCourses(), "Course")
    ch.selectCourse(course_selection)
    print(util.colored("Grading " + ch.selected_course.name, "lightgreen"))

    assn_selection = util.get_selection(ch.getAssignments(), "Assignment")
    ch.selectAssignment(assn_selection)
    print()

    print("Downloading submissions...")
    submissions = ch.getSubmissions()

    # Test Suite Creation
    testsuite = TestSuite.CreateWith(json_file=DEFAULT_TEST_FILE)

    # Grading
    pg = PythonGrader(submissions, testsuite)
    option = input("Grade All students [ENTER] or [s]election? ").lower()
    if option == "s":
        print("Downloading Student data...")
        student_selection = ch.getStudentSubset()
        pg.limitStudentsTo(student_selection)
    results = pg.getResults()

    yn = input("\nShow final report? [y/N] ").lower()
    if yn == "y":
        # Final report for manual grade checking
        total_report = len(results)
        index = 1
        print("\nFailed Grades ({}):\n".format(total_report))
        for user in util.lastname_lex(results.keys()):
            result = results[user]
            missingMsg = MISSING if result.missing else ""
            print(
                util.colored("({}/{})".format(index, total_report), "red"),
                util.colored(user.name, "white"),
                util.colored(user.id, "purple"),
                missingMsg)
            print(util.colored(result, "white"), "\n")
            index += 1
            q = input(REPORT_MESSAGE)
            print()
            if q == "q":
                break

    yn = input(
        "\n{} results collected. Upload grades? [y/N] ".format(
            len(results))).lower()
    if yn == "y":
        # Grade uploading
        print(GRADE_UPLOAD_REPORT)
        for user, result in results.items():
            grade = result.grade
            print(user.name, grade, end=" ")
            submission_successful = ch.postSubmissionResult(user, result)
            if submission_successful:
                print(SUCCESS)
            else:
                print(FAIL)
    print(GOODBYE)


if __name__ == "__main__":
    main()
